import re

from django.contrib.messages import INFO as INFO_LEVEL
from django.contrib.messages import SUCCESS as SUCCESS_LEVEL
from django.test import override_settings
from django.urls import reverse

from pytest_django.asserts import assertRedirects

from ..models import User
from .factories import UserFactory
from .utils import ViewMixin

HOME_URL = reverse('home')
LOGIN_URL = reverse('account:login')
RE_URL = re.compile(r'http(|s)://[^/]*/([^ ]*/)$', re.MULTILINE)


class TestLoginLogout(ViewMixin):
    url = LOGIN_URL

    def test_login(self, test_user):
        auth_form = self.get().form
        auth_form['username'] = 'user@example.org'
        auth_form['password'] = 'password'
        response = auth_form.submit()
        assertRedirects(response, reverse('home'))

    def test_login_inactive(self, test_user):
        test_user.is_active = False
        test_user.save()

        auth_form = self.get().form
        auth_form['username'] = 'user@example.org'
        auth_form['password'] = 'password'
        response = auth_form.submit()
        assert response.status_code == 200
        assert response.context['form'].errors

    def test_logout(self, test_user):
        response = (
            self.get(HOME_URL, user=test_user).forms['logout-form'].submit()
        )
        assertRedirects(response, self.url)
        # check that a success message has been added
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL


class TestPasswordReset(ViewMixin):
    url = reverse('account:password_reset')

    def test_workflow(self, mailoutbox):
        user = UserFactory(email='iforgot@example.org', password='')

        # 1. We request a password reset link for the user
        reset_form = self.get().form
        reset_form['email'] = user.email
        response = reset_form.submit()
        # … we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # … with an information message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # … an email has been sent
        assert len(mailoutbox) == 1
        # … with the password reset link.
        confirm_url = RE_URL.search(mailoutbox[0].body).group()
        assert confirm_url

        # 2. We follow the link and confirm the reset
        reset_form = self.get(confirm_url).follow().form
        reset_form['new_password1'] = 'BellaCiao'
        reset_form['new_password2'] = 'BellaCiao'
        response = reset_form.submit()
        # … we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # … we can now sign in with the new password.
        assert response.client.login(username=user.email, password='BellaCiao')

        # 3. The link is not valid anymore
        response = self.get(confirm_url)
        assert not response.forms

    def test_email_unknown(self, mailoutbox):
        # We request a password reset link for an unknown user
        reset_form = self.get().form
        reset_form['email'] = 'idontexist@no.lan'
        response = reset_form.submit()
        # … we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # … with an information message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # … but no email has been sent.
        assert len(mailoutbox) == 0

    def test_logged_in(self, test_user):
        response = self.get(user=test_user)
        assertRedirects(response, reverse('account:password_change'))


class TestRegister(ViewMixin):
    url = reverse('account:register')
    closed_url = reverse('account:registration_closed')

    def test_workflow(self, mailoutbox):
        password = 'SkaYiddish'

        # 1. We register an account
        form = self.get(status=200).form
        form['email'] = 'camille@dupont.org'
        form['password1'] = password
        form['password2'] = password
        form['first_name'] = 'Camille'
        form['last_name'] = 'Dupont'
        response = form.submit()
        # … we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # … with a success message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        # An email has been sent
        assert len(mailoutbox) == 1
        # … with the activation link
        activate_url = RE_URL.search(mailoutbox[0].body).group()
        assert activate_url
        # An inactive user has been created
        user = User.objects.get(email='camille@dupont.org')
        assert user.is_active is False
        # … and we can't log in yet.
        login_form = self.get(LOGIN_URL).form
        login_form['username'] = user.email
        login_form['password'] = password
        response = login_form.submit(status=200)
        assert (
            "Your account is inactive"
            in response.context['form'].non_field_errors()[0]
        )

        # 2. We follow the activation link
        response = self.get(activate_url)
        # … we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # … with a success message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        # The user is now active
        user.refresh_from_db()
        assert user.is_active is True
        # … and we can log in.
        login_form = self.get(LOGIN_URL).form
        login_form['username'] = user.email
        login_form['password'] = password
        response = login_form.submit()
        assertRedirects(response, HOME_URL)

        # 3. The link is not valid anymore
        response = self.get(activate_url, status=200)
        assert "Activation failed" in response

    @override_settings(REGISTRATION_OPEN=False)
    def test_registration_closed(self):
        response = self.get()
        assertRedirects(response, self.closed_url)
        response.follow(status=200)

    def test_registration_opened(self):
        response = self.get(self.closed_url)
        assertRedirects(response, self.url)
        response.follow(status=200)

    def test_unique_email(self, test_user):
        form = self.get().form
        form['email'] = 'User@example.org'
        form['password1'] = 'SkaYiddish'
        form['password2'] = 'SkaYiddish'
        form['first_name'] = 'Camille'
        form['last_name'] = 'Dupont'
        response = form.submit(status=200)

        assert response.context['form'].errors['email'][0] == (
            "A user with this email address already exists."
        )


class TestPasswordChange(ViewMixin):
    url = reverse('account:password_change')

    def test_unauthorized(self):
        response = self.get()
        assert response.status_code == 302

    def test_change(self, test_user):
        change_form = self.get(user=test_user).forms[1]
        change_form['old_password'] = 'password'
        change_form['new_password1'] = 'BellaCiao'
        change_form['new_password2'] = 'BellaCiao'
        response = change_form.submit()
        # we are redirected and the password has changed
        assertRedirects(response, reverse('account:profile_update'))
        assert response.client.login(
            username=test_user.email, password='BellaCiao'
        )


class TestProfileUpdate(ViewMixin):
    url = reverse('account:profile_update')

    def test_unauthorized(self):
        response = self.get()
        assert response.status_code == 302

    def test_edit(self, test_user):
        response = self.get(user=test_user)
        assert set(response.context['form'].fields.keys()) == {
            'first_name',
            'last_name',
        }
        # make changes and submit the form
        edit_form = response.forms[1]
        edit_form['first_name'] = 'Toto'
        response = edit_form.submit()
        assertRedirects(response, self.url)
        # check that the first name has changed
        test_user.refresh_from_db()
        assert test_user.first_name == 'Toto'
